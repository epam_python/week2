# week2

# Task 1

1) список квадратов всех позитивных значений типа int которые делятся на 2 без
остатка используя List Comprehensions-> list

2) список кубов всех негативных значений типа float которые НЕ делятся на 2 без
остатка, количество знаков после запятой: 2 используя List Comprehensions -> list

3) собрать предложение из слов используя ф-ю is_number используя List
Comprehensions -> str sentence

4) посчитать сумму всех негативных значений используя ф-ю is_number используя
List Comprehensions -> float

5) используя dict comprehensions получить словарь из dict_1 в котором ключом
будет имя а значением будет строка содрежащая через запятую два последних скила
в случае если персонаж является джедаем, в обратном случае значением
должна быть роль.

7) используя ф-ю sorter, отсортировать даные из словаря dict_1 в переменную
sort_by_height_desc, значения должны быть
отсортированы по росту в порядке убывания.

8) написать генератор, который будет принимать на вход переменную
sort_by_height_desc и итерировать ее содержимое, генератор должен быть
написан двух видах:
    - в виде функции
    - используя generator expression
присвоить генераторы переменным gen_1, gen_2

9) используя луп for, вывести содержимое обоих генераторов в консоль.


is_number = lambda x: isinstance(x, int) or isinstance(x, float)

list_1 = [
    "Comprehensions", 1, -1, "some", 7, 58.8, "34.46", "times ", -2, 3.6,
    -78.92, "123.1", "are", -8, "better", 3, "-46.78", 345, "than", -13.3,
    "'for'", 1.2, 9, 23.8, "loops.", 4, 5
]

dict_1 = {
    "Chewie": {
        "sex": "male",
        "age": 300,
        "height": 2.2,
        "role": "pilot",
        "skills": [
            "strenght", "extra hair", "reticence"
        ]
    },
    "Anakin Skywalker": {
        "sex": "male",
        "age": 22,
        "height": 1.8,
        "role": "jedi",
        "skills": [
            "speed", "high level of midichlorians", "honesty"
        ]
    },
    "Padme Amidala": {
        "sex": "female",
        "age": 19,
        "height": 1.6,
        "role": "queen",
        "skills": [
            "honesty", "trick", "bravery"
        ]
    },
    "R2D2": {
        "sex": "unknown",
        "age": 567,
        "height": 1.1,
        "role": "robot",
        "skills": [
             "bravery", "technical skills", "size"
        ]
    },
    "Yoda": {
        "sex": "male",
        "age": 450,
        "height": 0.9,
        "role": "jedi",
        "skills": [
             "high level of midichlorians", "wisdom", "experience"
        ]
    },
    "Sabe": {
        "sex": "female",
        "age": 15,
        "height": 1.7,
        "role": "servant",
        "skills": [
            "reticence", "sensitivity", "honesty"
        ]
    },
}




# Task 2
1)	Напишите coroutine, которая на вход будет принимать сообщение, и возвращать последние 5 сообщений. Допустим, это будут выполненные задачи или имена отработанных функций
2)	Создать 10к объектов, замерять потребляемое кол-во памяти (memory_profiler). Удалить каждый второй элемент, замерять потребляемое кол-во памяти. Вывести кол-во ссылок в каждом из случаев
3)	Возвести в степень элементы одного итерируемого объекта, взяв степень из другого итерируемого объекта с помощью map
4)	Сделать словарь с помощью zip с 2х lists
5)	Сделать словарь с помощью zip с 2х tuples
6)	Сделать словарь с помощью zip с 2итерируемых объектов

