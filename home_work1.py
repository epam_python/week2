list_1 = [
    'Comprehensions', 1, -1, "some", 7, 58.8, "34.46", "times ", -2, 3.6,
    -78.92, "123.1", "are", -8, "better", 3, "-46.78", 345, "than", -13.3,
    "'for'", 1.2, 9, 23.8, "loops.", 4, 5
    ]

dict_1 = {
    "Chewie": {
        "sex": "male",
        "age": 300,
        "height": 2.2,
        "role": "pilot",
        "skills": [
            "strenght", "extra hair", "reticence"
            ]
        },
    "Anakin Skywalker": {
        "sex": "male",
        "age": 22,
        "height": 1.8,
        "role": "jedi",
        "skills": [
            "speed", "high level of midichlorians", "honesty"
            ]
        },
    "Padme Amidala": {
        "sex": "female",
        "age": 19,
        "height": 1.6,
        "role": "queen",
        "skills": [
            "honesty", "trick", "bravery"
            ]
        },
    "R2D2": {
        "sex": "unknown",
        "age": 567,
        "height": 1.1,
        "role": "robot",
        "skills": [
            "bravery", "technical skills", "size"
            ]
        },
    "Yoda": {
        "sex": "male",
        "age": 450,
        "height": 0.9,
        "role": "jedi",
        "skills": [
            "high level of midichlorians", "wisdom", "experience"
            ]
        },
    "Sabe": {
        "sex": "female",
        "age": 15,
        "height": 1.7,
        "role": "servant",
        "skills": [
            "reticence", "sensitivity", "honesty"
            ]
        },
    }

is_number = lambda x: isinstance(x, int) or isinstance(x, float)


'1'
print([p ** 2 for p in list_1 if isinstance(p, int) if p >= 0 if p % 2 == 0])

'2'
print([p ** 3 for p in list_1 if isinstance(p, float) if p < 0 if p %
       2 != 0])

'3'
print(" ".join([x for x in list_1 if not (is_number(x))]))

'4'
print(sum([float(x) for x in list_1 if is_number(x) if x < 0]))

'5'
print({(name): (",".join(value["skills"][-2:]) if value["role"] == "jedi" else
                 value["role"]) for name, value in dict_1.items()})

'7'
sorted_keys = sorted(dict_1, key=lambda k: dict_1[k]["height"], reverse=True)
sort_by_height_desc = {key: dict_1[key] for key in sorted_keys}
print(sort_by_height_desc)


'8'
def gen_func(gen_1):
    return[gen_2 for gen_2 in gen_1]

print(gen_func(sort_by_height_desc))