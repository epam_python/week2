import random


'''
1)	Напишите coroutine, которая на вход будет принимать сообщение, и возвращать последние 5 сообщений. Допустим, это будут выполненные задачи или имена отработанных функций
2)	Создать 10к объектов, замерять потребляемое кол-во памяти (memory_profiler). Удалить каждый второй элемент, замерять потребляемое кол-во памяти. Вывести кол-во ссылок в каждом из случаев
3)	Возвести в степень элементы одного итерируемого объекта, взяв степень из другого итерируемого объекта с помощью map
4)	Сделать словарь с помощью zip с 2х lists
5)	Сделать словарь с помощью zip с 2х tuples
6)	Сделать словарь с помощью zip с 2итерируемых объектов
'''

'1'
def coroutine(s):
    def wrap(*args, **kwargs):
        gen = s(*args, **kwargs)
        gen.send(None)
        return gen
    return wrap


@coroutine
def hist():
    history = []
    while True:
        x, y = (yield)
        if y == 'h':
            print(history)
            continue
        result = x
        print(result)
        history.append(result)

'2'
class Item():
    def __init__(self):
        self.value = random.random()

garb1 = [Item() for x in range(10000)]
garb1 = [garb1[i] for i in range(0, len(garb1), 2)]
print(garb1)

'''=========================================================================='''

l1 = [1,2,3,4,5,6,7]
l2 = [7,6,5,4,3,2,1]
t1 = (1,2,3,4,5,6,7)
t2 = (7,6,5,4,3,2,1)

'3'
print(list(map(pow, l1, l2)))

'4'
print(dict(zip(l1, l2)))

'5'
print(dict(zip(t1, t2)))

'6'
print(dict(zip(l1, t2)))
